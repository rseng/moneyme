import React, {useState} from 'react';

import './ExpenseForm.css'

const ExpenseForm = (props) => {
    const [enteredTitle, setEntereredTitle] = useState('');
    const [enteredAmount, setEnteredAmount] = useState('');
    const [enteredDate, setEnteredDate] = useState('');

    const titleChangeHandler = (event) => {
        setEntereredTitle(event.target.value);
    };

    const amountChangeHandler = (event) => {
        setEnteredAmount(event.target.value);
    };

    const dateChangeHandler = (event) =>{
        setEnteredDate(event.target.value);

    };

    const submitHandler = (event) => {;
        event.preventDefault(); //this will prevent submitting the form (onSubmit) on line 34 from refreshing the page

        const expenseData ={
            title: enteredTitle,
            amount: enteredAmount,
            date: new Date(enteredDate) //Note : this is a date object that can have date methods applied to it
        };

        props.onSaveExpenseData(expenseData);

        setEntereredTitle('');
        setEnteredAmount('');
        setEnteredDate('');
    };
    return <form onSubmit={submitHandler}>
        <div className='new-expense__controls'>
            <div className='new-expense__control'>
                <label>Title</label>
                <input
                    onChange={titleChangeHandler}
                    type='text'
                    value={enteredTitle}
                />
            </div>
            <div className='new-expense__control'>
                <label>Amount</label>
                <input
                    onChange={amountChangeHandler}
                    type='number'
                    min="0.01"
                    step="0.01"
                    value={enteredAmount}
                />
            </div>
            <div className='new-expense__control'>
                <label>Date</label>
                <input
                    onChange={dateChangeHandler}
                    type='date'
                    min="2019-01-01"
                    max="2023-12-31"
                    value={enteredDate}
                />
            </div>
        </div>
        <div className="new-expense__actions">
            <button type="button" onClick={props.onCancel}>Cancel</button>
            <button type="submit">Add Expense</button>
        </div>
    </form>
};

export default ExpenseForm
